@extends('layouts.app')

@section('content')
    <style>
        .sender-message {
            background-color: #dcf8c6;
            padding: 10px;
            border-radius: 10px;
            margin-bottom: 10px;
            text-align: left;
            margin-right: 80px;
        }

        .receiver-message {
            background-color: #f1f0f0;
            padding: 10px;
            border-radius: 10px;
            margin-bottom: 10px;
            text-align: right;
            margin-left: 80px;
            margin-right: 10px;
        }

        .sender-name {
            font-weight: bold;
        }

        .message-text {
            margin-top: 5px;
        }

        .time-text {
            margin-top: 5px;
            font-size: 11px;;
        }

    </style>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">
                        Groups
                    </div>
                    <div class="card-body">
                        <ul class="list-group contact-list">
                            @foreach($groups as $group)
                                <a href="#" class="group" data-group-id="{{$group->id}}" data-group-name="{{$group->name}}">
                                    <li class="list-group-item">
                                        <div class="group-name">{{$group->name}}</div>
                                    </li>
                                </a>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <span id="chat-group-name"></span>
                    </div>
                    <div class="card-body">
                        <ul class="chat-messages" id="chat-messages" style="height: 50vh; overflow-y: scroll;">
                            <!-- list chat -->
                        </ul>
                    </div>
                    <div class="card-footer">
                        <div class="input-group">
                            <input type="text" class="form-control" id="chat-message-input" placeholder="Type your message..." disabled>
                            <input type="hidden" id="chat-group-input" value="">
                            <div class="input-group-append">
                                <button class="btn btn-primary" id="chat-send-button" disabled>Send</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $(document).ready(function () {
            const Echo = window.Echo
            var input = document.getElementById('chat-message-input')
            

            input.addEventListener('keypress', function(event) {
                if (event.key === 'Enter') {
                    event.preventDefault();
                    $('#chat-send-button').trigger('click')
                    $('#chat-message-input').val('')
                }
            })

            $('.group').click(function(){
                var groupName = $(this).attr('data-group-name')
                var groupId = $(this).attr('data-group-id')
                channelListener(groupId)

                $('#chat-group-name').text(groupName)
                $('#chat-group-input').val(groupId)

                $('.list-group-item').removeClass('active')
                var item = $(this).find('.list-group-item')
                $(item).addClass('active')

                $('#chat-message-input').prop('disabled', false)
                $('#chat-send-button').prop('disabled', false)

                $('#chat-messages').empty()
                var ajaxurl = "{{route('chat.load', ':groupId')}}";
                ajaxurl = ajaxurl.replace(':groupId', groupId);
                $.ajax({
                    url: ajaxurl,
                    method: 'GET',
                    success: function(response) {
                        var data = response.data
                        var userId = '{!! Auth::user()->id !!}'
                        $.each(data, function(key, value){
                            if(value.user_id == userId) {
                                $('#chat-messages').append(`
                                    <div class="message">
                                        <div class="receiver-message">
                                            <p class="message-text">`+value.message+`</p>
                                            <p class="time-text">`+value.sent_at+`</p>
                                        </div>
                                    </div>
                                `)
                            } else {
                                $('#chat-messages').append(`
                                    <div class="sender-message">
                                        <p class="sender-name">`+value.user.name+`</p>
                                        <p class="message-text">`+value.message+`</p>
                                        <p class="time-text">`+value.sent_at+`</p>
                                    </div>
                                `)
                            }
                        })

                        var messageContainer = document.getElementById('chat-messages');
                        messageContainer.scrollTo(0, messageContainer.scrollHeight);
                    },
                    error: function(xhr, status, error) {
                        console.log(error);
                    }
                });
                
            })

            $('#chat-send-button').click(function(){
                var ajaxurl = "{{route('chat.send')}}";
                var message = $('#chat-message-input').val()
                var groupId = $('#chat-group-input').val()
                if (groupId) {
                    $.ajax({
                        url: ajaxurl,
                        method: 'POST',
                        data: {
                            _token: '{{ csrf_token() }}',
                            group_id: groupId,
                            message: message
                        },
                        success: function(response) {
                            
                        },
                        error: function(xhr, status, error) {
                            console.log(error);
                        }
                    });
                }
            })

           

            function channelListener(groupId) {
                if (groupId) {
                    Echo.leave('chat.'+groupId)
                    
                    let channel = Echo.join('chat.'+groupId)
                    var userId = '{!! Auth::user()->id !!}'
                    channel.listen('SendMessage', function(data){
                        var groupMessage = data.groupMessage
                        if(groupMessage.user_id == userId) {
                            $('#chat-messages').append(`
                                <div class="message">
                                    <div class="receiver-message">
                                        <p class="message-text">`+groupMessage.message+`</p>
                                        <p class="time-text">`+groupMessage.sent_at+`</p>
                                    </div>
                                </div>
                            `)
                        } else {
                            $('#chat-messages').append(`
                                <div class="sender-message">
                                    <p class="sender-name">`+groupMessage.user.name+`</p>
                                    <p class="message-text">`+groupMessage.message+`</p>
                                    <p class="time-text">`+groupMessage.sent_at+`</p>
                                </div>
                            `)
                        }
                    })
                }
            }

            var groupId = $('#chat-group-input').val()
            channelListener(groupId)
        });
    </script>
@endsection
