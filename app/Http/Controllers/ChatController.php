<?php

namespace App\Http\Controllers;

use App\Events\SendMessage;
use App\Models\Group;
use App\Models\GroupMessage;
use Illuminate\Http\Request;

class ChatController extends Controller
{
    public function index() {
        $data['groups'] = Group::all();

        return view('pages.chat', $data);
    }

    public function sendMessage(Request $request) {
        $groupId = $request->group_id;
        $userId = auth()->user()->id;
        $message = $request->message;
        $groupMessage = GroupMessage::create([
            'group_id' => $groupId,
            'user_id' => $userId,
            'message' => $message,
        ]);
        
        broadcast(new SendMessage($groupMessage->user()->associate(auth()->user())));

        return response()->json([
            'success' => true,
            'message' => "success"
        ]);
    }

    public function loadMessage($groupId) {
        $messages = GroupMessage::with(['group', 'user'])->where('group_id', $groupId)->orderBy('created_at', 'asc')->get();

        return response()->json([
            'success' => true,
            'data' => $messages
        ]);
    }
}
