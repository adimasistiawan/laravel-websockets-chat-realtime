<?php

use App\Http\Controllers\AuthLoginController;
use App\Http\Controllers\ChatController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [AuthLoginController::class, 'showLoginForm']);
Route::post('/login', [AuthLoginController::class, 'login'])->name('login');
Route::post('/logout', [AuthLoginController::class, 'logout'])->name('logout');

Route::middleware('auth')->group(function () {
    Route::prefix('chat')->name('chat.')->group(function () {
        Route::get('/', [ChatController::class, 'index'])->name('index');
        Route::get('/load/{groupId}', [ChatController::class, 'loadMessage'])->name('load');
        Route::post('/send', [ChatController::class, 'sendMessage'])->name('send');
    });
});