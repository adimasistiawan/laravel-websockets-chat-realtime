<?php

namespace Database\Seeders;

use App\Models\Group;
use App\Models\GroupMember;
use App\Models\User;
use Illuminate\Database\Seeder;

class GroupMemberSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::pluck('id')->toArray();
        $groups = Group::pluck('id')->toArray();

        $groupMembers = [];

        foreach ($groups as $groupId) {
            foreach ($users as $userId) {
                $groupMembers[] = [
                    'group_id' => $groupId,
                    'user_id' => $userId,
                ];
            }
        }

        foreach ($groupMembers as $groupMember) {
            GroupMember::create($groupMember);
        }
    }
}
