<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\User\Role;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->name = "Rara";
        $user->email = "rara@gmail.com";
        $user->password = bcrypt('password');
        $user->save();

        $user = new User();
        $user->name = "Jaka";
        $user->email = "jaka@gmail.com";
        $user->password = bcrypt('password');
        $user->save();
    }
}