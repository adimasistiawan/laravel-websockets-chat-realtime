# Laravel WebSockets Chat Realtime Demo

This is a demo application built with the [Laravel WebSockets](https://github.com/beyondcode/laravel-websockets) package.

Be sure to check out the [official documentation](https://docs.beyondco.de/laravel-websockets/).

## Usage

1. Clone this repository
2. `composer install`
3. `cp .env.example .env`
4. Set the following variables in the `.env` file:

   ```dotenv
   PUSHER_APP_ID=your_app_id
   PUSHER_APP_KEY=your_app_key
   PUSHER_APP_SECRET=your_app_secret
5. `php artisan migrate`
6. `php artisan db:seed`
7. `php artisan key:generate`
8. `npm install`
9. `npm run dev`
10. `php artisan websockets:serve`
